# eks-cluster

### use existing VPC
Why: reduce cost by managin cheap node as instance
https://eksctl.io/usage/vpc-networking/

Existing VPC is managed by terraform

### command to create a cluster
eksctl create cluster -f cluster.yaml

### command to create nodegroups
eksctl create nodegroup --config-file=cluster.yaml

### command to scale node
eksctl scale nodegroup --cluster=eks-ahb --nodes=2 --name=ng-app-spot --region us-east-1

### command to delete nodegroup
eksctl delete nodegroup --config-file=cluster.yaml --include=ng-app-spot-2 --approve

### command to check nodegroup
eksctl get nodegroup --cluster eks-ahb --region us-east-1

### command to make new conf
aws eks update-kubeconfig --region us-east-1 --name eks-ahb --profile programboy

https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html

## Caution

eksctl delete cluster -f cluster.yaml