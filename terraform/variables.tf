provider "aws" {
  region  = "us-east-1"
  profile = "programboy"
}

terraform {
  required_providers {
    aws = {
      version = "~> 3.36.0"
      source  = "hashicorp/aws"
    }
  }

  required_version = ">= 1.0.11"
}

locals {
  eks_name = "eks-ahb"
}
