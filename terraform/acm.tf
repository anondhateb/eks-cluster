resource "aws_acm_certificate" "hbm" {
  domain_name       = "hbm.programmeboy.com"
  validation_method = "DNS"

  tags = {
    Environment = "hbm"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "hbm" {
  certificate_arn         = aws_acm_certificate.hbm.arn
  validation_record_fqdns = [for record in aws_route53_record.acm : record.fqdn]
}