## destroy the resrouces with terraform
I need to destroy the following resources manually.

### cloudfront
I tried to destroy cloudfront with terraform, but it made the cloudfront disable. I ended up destroying it manually.

### amazon s3
I tried to destroy s3 with terraform, but it was not empty. I need to delete all objects manually.

### note
Some of parts of the resources, which are data resources, have to be comment out because they are already destroyed.
