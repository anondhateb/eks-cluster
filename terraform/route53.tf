resource "aws_route53_zone" "hbm" {
  name = "hbm.programmeboy.com"

  tags = {
    Environment = "hbm"
  }
}

resource "aws_route53_record" "ns" {
  allow_overwrite = true
  name            = "hbm.programmeboy.com"
  ttl             = 86400
  type            = "NS"
  zone_id         = aws_route53_zone.hbm.zone_id

  records = [
    aws_route53_zone.hbm.name_servers[0],
    aws_route53_zone.hbm.name_servers[1],
    aws_route53_zone.hbm.name_servers[2],
    aws_route53_zone.hbm.name_servers[3],
  ]
}

resource "aws_route53_record" "soa" {
  allow_overwrite = true
  name            = "hbm.programmeboy.com"
  ttl             = 1800
  type            = "SOA"
  zone_id         = aws_route53_zone.hbm.zone_id

  records = [
    "${aws_route53_zone.hbm.name_servers[2]} awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"
  ]
}

resource "aws_route53_record" "acm" {
  for_each = {
    for o in aws_acm_certificate.hbm.domain_validation_options : o.domain_name => {
      name   = o.resource_record_name
      record = o.resource_record_value
      type   = o.resource_record_type
    }
  }

  name    = each.value.name
  ttl     = 300
  type    = each.value.type
  zone_id = aws_route53_zone.hbm.zone_id

  records = [each.value.record]
}

resource "aws_route53_record" "hbm" {
  name    = "hbm.programmeboy.com"
  type    = "A"
  zone_id = aws_route53_zone.hbm.zone_id

  alias {
    name                   = aws_cloudfront_distribution.hbm.domain_name
    zone_id                = aws_cloudfront_distribution.hbm.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "hbm_api" {
  name    = "api.hbm.programmeboy.com"
  type    = "A"
  zone_id = aws_route53_zone.hbm.zone_id

  #  alias {
  #    name                   = data.aws_lb.hbm.dns_name
  #    zone_id                = data.aws_lb.hbm.zone_id
  #    evaluate_target_health = true
  #  }
}

resource "aws_route53_record" "hbm_api_acme" {
  name    = "_acme-challenge.api.hbm.programmeboy.com"
  ttl     = 300
  type    = "TXT"
  zone_id = aws_route53_zone.hbm.zone_id

  records = ["1qH6dtQdmxZnkZye7RWl_h01HfmlHM1J4JkmfSBo8Z8"]
}
