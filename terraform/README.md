## A command to know ISSUER_URL
ISSUER_URL=$(aws eks describe-cluster --name eks-ahb \
  --query "cluster.identity.oidc.issuer" --region us-east-1 --output text)

## Note
[Amazon EKS の Amazon EC2 ノードグループで AWS Load Balancer Controller を使用して Application Load Balancer を設定するにはどうすればよいですか?](https://aws.amazon.com/jp/premiumsupport/knowledge-center/eks-alb-ingress-controller-setup/)