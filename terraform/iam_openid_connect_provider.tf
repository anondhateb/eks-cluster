resource "aws_iam_openid_connect_provider" "default" {
  # url = data.aws_eks_cluster.eks_ahb.identity[0].oidc[0].issuer
  url = "https://oidc.eks.us-east-1.amazonaws.com/id/3E0AA21AA3E4FC8E2809E4A2B0368996"

  client_id_list = [
    "sts.amazonaws.com",
  ]

  thumbprint_list = ["9e99a48a9960b14926bb7f3b02e22da2b0ab7280"]
}
