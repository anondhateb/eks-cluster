### load-balancer-control ###############################################################

resource "aws_iam_policy" "load_balancer_control" {
  name        = "load_balancer_control"
  path        = "/"
  description = "load balancer control"

  policy = file("iam-policy-load-balancer-control.json")
}

output "load_balancer_controll_arn" {
  value = aws_iam_policy.load_balancer_control.arn
}

### external-dns ###############################################################

resource "aws_iam_policy" "external_dns" {
  name        = "external_dns"
  path        = "/"
  description = "load external_dns control"

  policy = file("iam-policy-external-dns.json")
}

output "external_dns_arn" {
  value = aws_iam_policy.external_dns.arn
}