resource "aws_cloudfront_distribution" "hbm" {
  origin {
    domain_name = "hbm.programmeboy.com.s3-website-us-east-1.amazonaws.com"
    origin_id   = "hbm"

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy   = "http-only"
      origin_read_timeout      = 20
      origin_ssl_protocols     = [
        "TLSv1.2",
      ]
    }
  }

  aliases = [
    "hbm.programmeboy.com"
  ]

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "hbm"

    forwarded_values {
      query_string = true

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 180
    max_ttl                = 300
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = "hbm"
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.hbm.arn
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1.2_2019"
  }

  retain_on_delete = "true"
}