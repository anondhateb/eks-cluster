### load-balancer-control ###############################################################

resource "aws_iam_role" "oidc" {
  name = "oidc"
  path = "/"

  assume_role_policy = file("iam-assume-policy-oidc.json")
}

resource "aws_iam_role_policy" "oidc" {
  name = "oidc"
  role = aws_iam_role.oidc.id

  policy = file("iam-policy-oidc.json")
}

resource "aws_iam_role_policy_attachment" "oidc_load_balancer_control" {
  role       = aws_iam_role.oidc.name
  policy_arn = aws_iam_policy.load_balancer_control.arn
}

### external-dns ###############################################################

resource "aws_iam_role" "external_dns" {
  name = "external_dns"
  path = "/"

  assume_role_policy = file("iam-assume-policy-external-dns.json")
}

resource "aws_iam_role_policy_attachment" "external_dns" {
  role       = aws_iam_role.external_dns.name
  policy_arn = aws_iam_policy.external_dns.arn
}