resource "aws_s3_bucket" "hbm" {
  bucket = "hbm.programmeboy.com"

  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket_policy" "hbm" {
  bucket = aws_s3_bucket.hbm.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid = "PublicReadGetObject",
        Effect = "Allow",
        Principal = "*",
        Action = "s3:GetObject",
        Resource = [
          aws_s3_bucket.hbm.arn,
          "${aws_s3_bucket.hbm.arn}/*",
        ]
      }
    ]
  })
}