## Install the Node Termination Handler
curl -LO https://github.com/aws/aws-node-termination-handler/releases/download/v1.13.0/all-resources.yaml

## Install the Cluster Autoscaler
curl -LO https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml

Due to small instance, I decide not to use cluster autoscalar.

## Install the cert-manager
curl -L -o cert-manager.yaml https://github.com/jetstack/cert-manager/releases/download/v1.3.1/cert-manager.yaml

## Install the AWS Load Balancer Controller
curl -L -o ingress-controller.yaml https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.3/docs/install/v2_1_0_full.yaml

## Install the 2048
curl -L -o 2048_full.yaml https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.3/docs/examples/2048/2048_full.yaml

## Install external DNS
curl -L -o external-dns.yaml https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.3/docs/examples/external-dns.yaml

'https://github.com/kubernetes-sigs/external-dns/issues/1899

I install extternal DNS. However, I do not use this one as it has a bug.
Intead I set dns using terraform. When you try this feature again, please read docs again.

## Note
[Building for Cost optimization and Resilience for EKS with Spot Instances](https://aws.amazon.com/jp/blogs/compute/cost-optimization-and-resilience-eks-with-spot-instances/)


[Amazon EKS の Amazon EC2 ノードグループで AWS Load Balancer Controller を使用して Application Load Balancer を設定するにはどうすればよいですか?](https://aws.amazon.com/jp/premiumsupport/knowledge-center/eks-alb-ingress-controller-setup/)

[Setup external-DNS to manage DNS automatically](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.2/examples/echo_server/#setup-external-dns-to-manage-dns-automatically)