## A certificate
Envoy uses a certificate and key when it's terminating SSL/TLS connections. 

## (Option) A self-signed SSL/TLS certificate and key:
```
EXTERNAL_IP=$(kubectl get service envoy -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

openssl req -x509 -nodes -newkey rsa:2048 -days 365 \
    -keyout privkey.pem -out cert.pem -subj "/CN=$EXTERNAL_IP"
```

## A SSL/TLS certificate and key using certbot
```
export MAIL_ADDRESS=xxxx

sudo certbot certonly --manual \
--preferred-challenges=dns \
-d api.hbm.programmeboy.com \
-m ${MAIL_ADDRESS}
```

## Secret of certificate should be generate using a self-sigined certificate
kubectl create secret tls envoy-certs \
    --key privkey.pem --cert cert.pem \
    --dry-run -o yaml | kubectl apply -f -

## Secret of certificate should be generate using a certbot certificate
kubectl create secret tls envoy-certs \
    --key privkey.pem --cert fullchain.pem \
    --dry-run -o yaml | kubectl apply -f -

## Note
Files in this direcotory should NOT be commited.
